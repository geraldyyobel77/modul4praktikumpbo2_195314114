
import java.net.URL;
import javax.swing.ImageIcon;
import javax.swing.JDialog;
import javax.swing.JLabel;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author lenovo
 */
public class lat3 extends JDialog {

    lat3() {

        JLabel image;
        JLabel text;
        setTitle("Text and Icon Picture");
        setSize(250, 300);
        this.setLayout(null);

        ImageIcon foto = new ImageIcon("1.png");
        image = new JLabel();
        image.setIcon(foto);
        image.setBounds(30, 10, 200, 200);
        this.add(image);
        image.setVisible(true);

        text = new JLabel("Grapes");
        text.setBounds(80, 200, 80, 40);
        this.add(text);
    }

    public static void main(String[] args) {
        lat3 picture = new lat3();
        picture.setVisible(true);

    }
}
