
import java.awt.Color;
import java.awt.Container;
import java.awt.FlowLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import static javax.swing.WindowConstants.EXIT_ON_CLOSE;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author lenovo
 */
public class lat2 extends JDialog {

    private static final int FRAME_WIDTH = 300;
    private static final int FRAME_HEIGHT = 200;
    private static final int FRAME_X_ORIGIN = 150;
    private static final int FRAME_Y_ORIGIN = 250;

    public static void main(String[] args) {
        lat2 frame = new lat2();
        frame.setVisible(true);

    }

    public lat2() {
        Container contentPane;
        JButton button1, button2, button3;

        setSize(FRAME_WIDTH, FRAME_HEIGHT);
        setTitle("Button Test");
        setLocation(FRAME_X_ORIGIN, FRAME_Y_ORIGIN);

        contentPane = getContentPane();
        contentPane.setBackground(Color.white);
        contentPane.setLayout(new FlowLayout());

        button1 = new JButton("Yellow");
        button2 = new JButton("Blue");
        button3 = new JButton("Red");

        contentPane.add(button1);
        contentPane.add(button2);
        contentPane.add(button3);
        setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
    }
}
