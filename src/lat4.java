
import java.awt.BorderLayout;
import java.awt.Checkbox;
import java.awt.Color;
import java.awt.Container;
import java.awt.GridLayout;
import java.awt.font.TextLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author lenovo
 */
public class lat4 extends JDialog {

    public lat4() {

        Container ContentPane = getContentPane();
        ContentPane.setBackground(Color.white);
        ContentPane.setLayout(new BorderLayout());
        setTitle("CheckboxDemo");
        setSize(500, 140);
        setLocation(150, 250);

        JLabel text = new JLabel("Welcome to Java");
        text.setBackground(Color.WHITE);
        JTextArea text1 = new JTextArea("Welcome to Java");
        text.add(text1);

        JPanel forCen = new JPanel(new GridLayout(3, 3));
        JCheckBox center = new JCheckBox("Centered");
        forCen.add(center);
        JCheckBox bold = new JCheckBox("Bold");
        forCen.add(bold);
        JCheckBox italic = new JCheckBox("Italic");
        forCen.add(italic);

        JPanel side = new JPanel();
        JButton left = new JButton("left");
        JButton right = new JButton("Right");
        side.add(left);
        side.add(right);

        ContentPane.add(text, BorderLayout.CENTER);
        ContentPane.add(forCen, BorderLayout.EAST);
        ContentPane.add(side, BorderLayout.SOUTH);

    }

    public static void main(String[] args) {
        lat4 box = new lat4();
        box.setVisible(true);
    }
}
