/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.event.ActionEvent;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;

/**
 *
 * @author lenovo
 */
public class lat1 extends JFrame {

    private JMenuBar menuBar;
    private JMenuItem menuItemFile_tampil1;
    private JMenuItem menuItemFile_tampil2;
    private JMenu menu_File;
    private JMenu menu_Edit;

    public lat1() {
        komponen();
    }

    private void komponen() {
        menuBar = new JMenuBar();
        menu_File = new JMenu("File");
        menu_Edit = new JMenu("Edit");

        menuBar.add(menu_File);
        menuBar.add(menu_Edit);
        this.setJMenuBar(menuBar);

        menuItemFile_tampil1 = new JMenuItem("Tampil1");
        menuItemFile_tampil2 = new JMenuItem("Tampil2");

        Container contentPane;
        contentPane = getContentPane();
        contentPane.setBackground(Color.PINK);
        contentPane.setLayout(new BorderLayout());

        menu_File.add(menuItemFile_tampil1);
        menu_File.add(menuItemFile_tampil2);

        menuBar.add(menu_File);
        menuBar.add(menu_Edit);

    }

    public static void main(String[] args) {
        lat1 main = new lat1();
        main.setSize(400, 500);
        main.setVisible(true);
        main.setResizable(false);
        main.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

}
