
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.GridLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextArea;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author lenovo
 */
public class lat5 extends JDialog {

    public lat5() {

        Container ContentPane = getContentPane();
        ContentPane.setBackground(Color.WHITE);
        ContentPane.setLayout(new BorderLayout());
        
        setTitle("CheckboxDemo");
        setSize(500, 140);
        setLocation(150, 250);

        JTextArea text = new JTextArea("Welcome to Java");
        JPanel panel1 = new JPanel();
        panel1.setBackground(Color.WHITE);
        panel1.add(text);

        JPanel panel2 = new JPanel(new GridLayout(3, 3));
        JCheckBox center = new JCheckBox("Centered");
        panel2.add(center);
        JCheckBox bold = new JCheckBox("Bold");
        panel2.add(bold);
        JCheckBox italic = new JCheckBox("Italic");
        panel2.add(italic);

        JPanel panel3 = new JPanel(new GridLayout(3, 3));
        JRadioButton red = new JRadioButton("Red");
        panel3.add(red);
        JRadioButton green = new JRadioButton("green");
        panel3.add(green);
        JRadioButton blue = new JRadioButton("blue");
        panel3.add(blue);

        JPanel panel4 = new JPanel();
        JButton left = new JButton("left");
        JButton right = new JButton("Right");
        panel4.add(left);
        panel4.add(right);

        ContentPane.add(panel1, BorderLayout.CENTER);
        ContentPane.add(panel2, BorderLayout.EAST);
        ContentPane.add(panel3, BorderLayout.WEST);
        ContentPane.add(panel4, BorderLayout.SOUTH);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
    }

    public static void main(String[] args) {
        lat5 box = new lat5();
        box.setVisible(true);
    }
}
