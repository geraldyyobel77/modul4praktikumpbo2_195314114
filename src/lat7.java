
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import static javax.swing.WindowConstants.EXIT_ON_CLOSE;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author lenovo
 */
public class lat7 extends JDialog {

    private static final int FRAME_WIDTH = 300;
    private static final int FRAME_HEIGHT = 200;
    private static final int FRAME_X_ORIGIN = 150;
    private static final int FRAME_Y_ORIGIN = 250;
    private JList list;

    public lat7() {
        Container contentPane;
        JPanel listPanel;
        String[] names = {"Canada", "China", "Denmark", "France", "Germany", 
            "India", "Norway", "United Kingdom", "USA"};

        setSize(FRAME_WIDTH, FRAME_HEIGHT);
        setTitle("List Demo");
        setLocation(FRAME_X_ORIGIN, FRAME_Y_ORIGIN);

        contentPane = getContentPane();
        contentPane.setBackground(Color.black);
        contentPane.setLayout(new BorderLayout());

        listPanel = new JPanel(new GridLayout(0, 1));
        list = new JList(names);
        listPanel.add(new JScrollPane(list));

        JLabel image = new JLabel();
        JPanel flow = new JPanel(new FlowLayout());
        ImageIcon canada = new ImageIcon("canada.png");
        image = new JLabel();
        image.setIcon(canada);
        this.add(image);
        image.setBounds(30, 10, 100, 100);
        image.setVisible(true);

        ImageIcon china = new ImageIcon("chn.png");
        JLabel image1 = new JLabel();
        image1.setIcon(china);
        this.add(image1);
        image1.setBounds(30, 10, 100, 100);
        image1.setVisible(true);

        ImageIcon germany = new ImageIcon("germ.png");
        JLabel image2 = new JLabel();
        image2.setIcon(germany);
        this.add(image2);
        image.setBounds(30, 10, 100, 100);
        image2.setVisible(true);

        ImageIcon india = new ImageIcon("ind.png");
        JLabel image3 = new JLabel();
        image3.setIcon(india);
        this.add(image3);
        image.setBounds(30, 10, 100, 100);
        image3.setVisible(true);

        ImageIcon uk = new ImageIcon("uk.png");
        JLabel image4 = new JLabel();
        image4.setIcon(uk);
        this.add(image4);
        image4.setVisible(true);

        ImageIcon usa = new ImageIcon("usa.png");
        JLabel image5 = new JLabel();
        image5.setIcon(usa);
        this.add(image5);
        image5.setVisible(true);

        flow.add(image);
        flow.add(image1);
        flow.add(image2);
        flow.add(image3);
        flow.add(image4);flow.add(image5);
        
        contentPane.add(flow);
        contentPane.add(listPanel, BorderLayout.WEST);
    }

    public static void main(String[] args) {
        lat7 main = new lat7();
        main.setVisible(true);
    }
}
